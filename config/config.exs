# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :users,
  ecto_repos: [Users.Repo]

# Configures the endpoint
config :users, UsersWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "aGZO9IWBeYlQkIFz3Ab5awTyzlaZq/5df798CzVYvaTzyM9rJfrBc7m6f3SScY04",
  render_errors: [view: UsersWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Users.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
